This document has useful instructions for people who are interested in fixing bugs and adding features in Wmfdata-Python. If you just want to _use_ this package, you won't need to do these things!


## Setting up for development
### Standard approach
Because this package's main purpose is accessing data within the Data Platform, it's generally easiest to do your development work on a [stat client](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Clients).

First, clone the repository to a location of your choice on your chosen stat client.

If you're not using the Jupyter terminal, make sure to activate your desired [Conda environment](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Conda) before continuing.

Then, install the cloned repository in [development mode](https://setuptools.pypa.io/en/latest/userguide/development_mode.html): `pip install -e {repository_location}`.

You're ready to make whatever changes you want! Note that, if you're testing your changes in a Python notebook and you've previously imported the module, your changes will not take effect until you restart your notebook kernel.

To run the integration test script: `python tests/integrations.py $(whoami)`.

If you want to you to change the dependencies or run the linting or test steps locally (they are also run automatically on all merge requests), you will need to set up a UV development environment outside your Conda environment. See the next section for details.

### Developing using UV

Wmfdata-Python uses [UV](https://github.com/astral-sh/uv) to manage dependencies and set up consistent environments for CI, so you can also use it to quickly and conveniently set up a local development environment.

You can use UV on the stat clients too, including with the same repository clone you're using in a Conda-Analytics environment. Just note that you need to follow these steps _outside_ a Conda-Analytics environment (e.g. in a non-Jupyter terminal session).

1. Install uv with `curl -LsSf https://astral.sh/uv/install.sh | sh` for your user (if you're on a stat client, remember to set the [HTTP proxy](https://wikitech.wikimedia.org/wiki/HTTP_proxy) first).
2. Create virtual env with `uv venv`, which uses .venv by default, and activate it with `source .venv/bin/activate`
3. Install project and its dependencies with `uv sync`

Example commands:
```
# Run unit tests
$ pytest
# Run linting
$ ruff check
# Update the lock file with new dependencies
$ uv lock
```

### Developing using Docker
Both options above require working on a stat client as Wmfdata has various system dependencies (Mariadb, Kerberos). However, it is not possible to work on changes that touch system dependencies on the stat clients (that requires either docker or root access). Instead, Docker can be used to develop locally; for example, it is much faster to iterate on the Blubber file locally, instead of pushing to GitLab and waiting for the CI to run.

1. Build a Docker image with all dependencies installed:
```
docker build --platform linux/amd64 --target development -f .pipeline/blubber.yaml . -t development
```
2. Get a terminal with the cloned repository mounted as a volume:
```
docker run -it --platform linux/amd64 -v $(pwd):/srv/app development /bin/bash
```

To run e.g. the unit tests in that terminal
```
somebody@bae095fad8f0:/srv/app$ source $UV_PROJECT_ENVIRONMENT/bin/activate"
(app) somebody@bae095fad8f0:/srv/app$ uv run ruff check
```

## Releasing a new version
1. Make sure you have origin-tracking versions of the `main` and `release` branches on your computer.
    * To check, run `git remote show origin`. The list under `Local refs configured for 'git push':` should include both `main` and `release`.
    * If you're missing one, use `git checkout --track origin/{{branch}}`. Delete your local version first if necessary (`git branch -d {{branch}}`)
3. Check out the `main` branch and make sure all the changes you want to release have been merged in.
4. Run the test script (`python tests/integrations.py {your_hive_database}`) to verify that all the core functionality works properly.
5. Decide the new version number. This package follows [Semantic Versioning](https://semver.org/):
    > Given a version number MAJOR.MINOR.PATCH, increment the: MAJOR version when you make incompatible API changes, MINOR version when you add functionality in a backwards compatible manner, and PATCH version when you make backwards compatible bug fixes.
6. Update the main version number in `pyproject.toml` and its copy in `wmfdata/metadata.py`.
7. Update `CHANGELOG.md` with all of the noteworthy changes in the release.
7. If there are any new user-facing methods, add them to the [quickstart notebook](quickstart.ipynb).
7. Reinstall your local copy of Wmfdata using `pip install -e`. This ensures that the quickstart notebook will display the new version number.
7. In any case, re-run the quickstart notebook to ensure that it's fully up to date.
8. Commit your changes using the commit message "Make version X.Y.Z".
9. Tag the commit you just made with the version (`git tag -a vX.Y.Z -m "version X.Y.Z"`).
10. Push the new commit and tag to the origin (`git push --follow-tags`).
11. Check out the `release` branch and rebase it onto `main`.
12. Now push this branch to the origin. This is the step that will trigger update notification to users.
14. If the release is significant, announce it to `analytics-announce@lists.wikimedia.org`.
