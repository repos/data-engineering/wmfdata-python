# Wmfdata ≤ 2.4.0 accesses this file to determine the latest available version,
# so it is kept for backwards compatibility.
#
# It can be removed when we are confident that all Wmfdata users have upgraded
# past 2.4.0.
version = "2.4.0"
